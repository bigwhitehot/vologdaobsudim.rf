<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template {
		var $template_data = array();
		
		function set($name, $value)
		{
			$this->template_data[$name] = $value;
		}
	
		function load($template = '', $view = '', $view_data = array(), $crud = FALSE, $return = FALSE)
		{               
			$this->CI =& get_instance();
			$this->set('contents', $this->CI->load->view($view, $view_data, TRUE));
			$this->set('crud_head', ($crud) ? $this->CI->load->view("crud_head", $view_data, TRUE) : "");
            $this->set('crud_footer', ($crud) ? $this->CI->load->view("crud_footer", $view_data, TRUE) : "");
			return $this->CI->load->view($template, $this->template_data, $return);
		}
}

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */