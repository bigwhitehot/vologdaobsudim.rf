<?php include('header.php'); ?>
    <div class="container content all-news">
        <?php foreach ($news as $new) : ?>
        	<div class="news_list">
	            <a href="<?= base_url('news/' . $new->url); ?>"><h3><?= $new->name; ?></h3></a>

	            <div class="date_news">
	            	<small>Опубликовано: <?=date('d.m.Y', strtotime($new->created_at));?> |
                        <?=date('h:i', strtotime($new->created_at));?></small>
                </div>
                <p>
                <?=mb_strimwidth($new->text, 0, 220, '...');?>
            	</p>
	        </div>
        <?php endforeach; ?>
    </div>
    <div class="block-empty">&nbsp;</div>
<?php include('footer.php'); ?>