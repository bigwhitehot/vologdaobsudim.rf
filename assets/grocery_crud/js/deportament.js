$(function() {

    var code_postal = 0;
    var max_count_user = 5;
    var arrayOpCount;

    _getFieldDeportament();

    $('#field-concessionnaire').change(function(){
        _getFieldDeportament();
    });

    $('#field_utilisateurs_chosen').bind("DOMSubtreeModified",function(){
        var _ch_count = $('.search-choice').length;
        //var _op_count = $('#field-utilisateurs').find('option').length;
        if(_ch_count >= max_count_user) $(this).find('.chosen-drop').css({'display':'none'});
        else $(this).find('.chosen-drop').css({'display':'block'});

       /* for(var i = 0; i < _op_count; i++) {
            arrayOpCount[i] = $('#field-utilisateurs').find('option').eq(i);
        }*/

    });

    function _getFieldDeportament() {
        $.ajax({
            'url':'/api/get/concessionnaires/'+$("#field-concessionnaire").val(),
            'success':function(response){
                if(response.id){
                    $("#field-adress").val(response.adress);
                    $("#field-city").val(response.city);
                    $("#field-code_postal").val(response.code_postal);
                    code_postal = response.code_postal;
                }else {
                    $("#field-adress").val("");
                    $("#field-city").val("");
                    $("#field-code_postal").val("");
                }
            }
        });
    }

});