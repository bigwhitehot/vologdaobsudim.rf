var myMap;

var verifyCallbackAuth = function (response) {
    $('#authModal button').removeAttr('disabled');
};

var verifyCallbackRegister = function (response) {
    $('#registerModal button').removeAttr('disabled');
};

var onloadCallback = function () {
    grecaptcha.render('html_element', {
        'sitekey': '6LdlBbgUAAAAABLvMtLeGD53O5TQ_y47F3ucZU4_',
        'callback': verifyCallbackAuth,
    });
    grecaptcha.render('html_element2', {
        'sitekey': '6LdlBbgUAAAAABLvMtLeGD53O5TQ_y47F3ucZU4_',
        'callback': verifyCallbackRegister,
    });
};

ymaps.ready(init);

function init() {
    myMap = new ymaps.Map('map', {
        center: [59.220496, 39.891523], // Москва
        zoom: 13,
        controls: ['zoomControl', 'searchControl', 'fullscreenControl'],
    });

    objectManager = new ymaps.ObjectManager({
        // Чтобы метки начали кластеризоваться, выставляем опцию.
        clusterize: true,
        // ObjectManager принимает те же опции, что и кластеризатор.
        gridSize: 32,
        clusterDisableClickZoom: true
    });

    $.ajax({
        url: '/site/ajax_trabl',
        success: function (data) {
            console.log(objectManager.add(data));
        }
    });

    myMap.geoObjects.add(objectManager);
    myMap.behaviors.disable('scrollZoom');
    myMap.events.add('click', function (e) {
        if (auth > 0) {
            var coords = e.get('coords');

            placemark = new ymaps.Placemark(coords, {
                balloonContent: 'Укажите вашу проблемуу'
            }, {
                preset: 'islands#icon',
                iconColor: '#0095b6'
            });

            // добавляем балун
            // myMap.geoObjects.add(placemark);

            ymaps.geocode(coords).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);

                $('#mapModal #address').text(firstGeoObject.getAddressLine());
                $('.address').val(firstGeoObject.getAddressLine());
                $('.coord').val(coords);
            });

            $('#mapModal').modal('show');

            // открываем балун
            // placemark.balloon.open();
        } else {
            $('#authModal').modal('show');
        }
    });

}

$(document).ready(function () {
    $('.custom-file-input').on('change', function () {
        var fileName = $(this).val();
        $('.custom-file-label').text(fileName);
    });

    $('.trabl a .like div, .trabl .like div').on('click', function () {
        var type = $(this).attr('class');
        var thisel = $(this)
        var trabl_id = $(this).data('ids');

        $.ajax({
            method: 'get',
            url: '/site/ajax_reit',
            data: {
                'type': type,
                'trabl_id': trabl_id
            },
            success: function (data) {
                data = JSON.parse(data);
                if (data.status === 'ok') {
                    var numb = parseInt($(thisel).children('span').html());
                    var setn = numb + 1;

                    $(thisel).children('span').html(setn);

                    return false;
                }
            }
        });

        return false;
    })
})