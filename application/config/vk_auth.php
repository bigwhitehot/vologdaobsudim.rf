<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['VK_AUTH_SECRET'] = 'vvEsb7PTwTOPrQxKIcUD';
$config['VK_AUTH_APPID'] = '7232006';
$config['VK_AUTH_REDIRECT_URL'] = 'http://vologdaobsudim.rf/site/vk_auth';

$config['VK_AUTH_OAUTH_URL'] =  'http://oauth.vk.com/authorize';

$config['VK_AUTH_PARAMS'] = [
    'client_id' => $config['VK_AUTH_APPID'],
    'redirect_uri'  => $config['VK_AUTH_REDIRECT_URL'], 
    'response_type' => 'code'
];
