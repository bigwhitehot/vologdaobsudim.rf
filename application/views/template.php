<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="theme-color" content="#FF5722">
        <title><?php echo config_item('project_name'); ?> | Панель управления</title>
        <!-- Favicon-->
        <link rel="icon" href="<?php echo base_url(); ?>adminBSB/favicon.ico" type="image/x-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <link href="<?php echo base_url(); ?>adminBSB/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

        <!-- Waves Effect Css -->
        <link href="<?php echo base_url(); ?>adminBSB/plugins/node-waves/waves.css" rel="stylesheet" />

        <link href="<?php echo base_url(); ?>adminBSB/plugins/morrisjs/morris.css" rel="stylesheet" />
        <!-- Animation Css -->
        <link href="<?php echo base_url(); ?>adminBSB/plugins/animate-css/animate.css" rel="stylesheet" />

        <!-- Custom Css -->
        <link href="<?php echo base_url(); ?>adminBSB/css/style.css" rel="stylesheet">

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="<?php echo base_url(); ?>adminBSB/css/themes/all-themes.css" rel="stylesheet" />

        <?php echo $crud_head; ?>

    </head>

    <body class="theme-deep-orange">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-deep-orange">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Пожалуйста подождите...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->
        <!-- Top Bar -->
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="<?php echo base_url(); ?>"><?php echo config_item('project_name'); ?></a>
                </div>
            </div>
        </nav>
        <!-- #Top Bar -->
        <section>
            <!-- Left Sidebar -->
            <aside id="leftsidebar" class="sidebar">
                <!-- User Info -->
                <div class="user-info">
                    <div class="image">
                        <img src="<?php echo base_url(); ?>adminBSB/images/user.png" width="48" height="48" alt="User" />
                    </div>
                    <div class="info-container">
                        <?php $user = $this->ion_auth->user()->row(); ?>
                        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo @$user->first_name.' '.@$user->last_name; ?></div>
                        <div class="email"><?php echo @$user->email;?></div>
                        <div class="btn-group user-helper-dropdown">
                            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="/control/users/edit/<?php echo @$user->id;?>"><i class="material-icons">person</i>Профиль</a></li>
                                <li role="seperator" class="divider"></li>
                                <li><a href="/auth/logout"><i class="material-icons">input</i>Выход</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #User Info -->
                <!-- Menu -->
                <div class="menu">
                    <ul class="list">

                        <?php @$page_current = ($_SERVER['REQUEST_URI']=='/') ? 'home' : explode('/', mb_substr($_SERVER['REQUEST_URI'], 1))[1]; ?>

                        <li class="header">Главное меню</li>
                        <li <?php if($page_current=='trabl'){ echo 'class="active"'; }?>>
                            <a href="<?php echo base_url('control/trabl'); ?>">
                                <i class="material-icons">book</i>
                                <span>Обращения</span>
                            </a>
                        </li>
                        <li <?php if($page_current=='comment'){ echo 'class="active"'; }?>>
                            <a href="<?php echo base_url('control/comment'); ?>">
                                <i class="material-icons">book</i>
                                <span>Комментарии</span>
                            </a>
                        </li>
                        <li <?php if($page_current=='news'){ echo 'class="active"'; }?>>
                            <a href="<?php echo base_url('control/news'); ?>">
                                <i class="material-icons">book</i>
                                <span>Новости</span>
                            </a>
                        </li>
                        <li <?php if($page_current=='people'){ echo 'class="active"'; }?>>
                            <a href="<?php echo base_url('control/people'); ?>">
                                <i class="material-icons">book</i>
                                <span>Пользователи</span>
                            </a>
                        </li>
                        <li class="header">Настройки</li>
                        <li <?php if($page_current=='users'){ echo 'class="active"'; }?>>
                            <a href="<?php echo base_url(); ?>control/users">
                                <i class="material-icons">account_box</i>
                                <span>Администраторы</span>
                            </a>
                        </li>
                        <li <?php if($page_current=='trabl_subject'){ echo 'class="active"'; }?>>
                            <a href="<?php echo base_url('control/trabl_subject'); ?>">
                                <i class="material-icons">settings</i>
                                <span>Темы обращений</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- #Menu -->
                <!-- Footer -->
                <div class="legal">
                    <div class="copyright">
                        &copy; <?=date('Y');?> <a href="javascript:void(0);"><?php echo config_item('project_company'); ?></a>.
                    </div>
                </div>
                <!-- #Footer -->
            </aside>
            <!-- #END# Left Sidebar -->

        </section>

        <section class="content">
            <?php
            echo($contents);
            ?>
        </section>

        <!-- Jquery Core Js -->
        <!-- <script src="<?php echo base_url(); ?>adminBSB/plugins/jquery/jquery.min.js"></script> -->
        <script src="<?php echo base_url(); ?>adminBSB/plugins/jquery/jquery-2.2.3.min.js"></script>

        <!-- Bootstrap Core Js -->
        <script src="<?php echo base_url(); ?>adminBSB/plugins/bootstrap/js/bootstrap.js"></script>

        <!-- Select Plugin Js
        <script src="<?php echo base_url(); ?>adminBSB/plugins/bootstrap-select/js/bootstrap-select.js"></script>-->

        <!-- Slimscroll Plugin Js -->
        <script src="<?php echo base_url(); ?>adminBSB/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="<?php echo base_url(); ?>adminBSB/plugins/node-waves/waves.js"></script>

        <!-- Jquery CountTo Plugin Js -->
        <script src="<?php echo base_url(); ?>adminBSB/plugins/jquery-countto/jquery.countTo.js"></script>

        <!-- Crud Js -->
        <?php echo $crud_footer; ?>

        <?php if($page_current=='home'){  ?>

        <!-- Morris Plugin Js -->
        <script src="<?php echo base_url(); ?>adminBSB/plugins/raphael/raphael.min.js"></script>
        <script src="<?php echo base_url(); ?>adminBSB/plugins/morrisjs/morris.js"></script>

        <!-- ChartJs -->
        <script src="<?php echo base_url(); ?>adminBSB/plugins/chartjs/Chart.bundle.js"></script>


            <!-- Flot Charts Plugin Js -->
            <script src="<?php echo base_url(); ?>adminBSB/plugins/flot-charts/jquery.flot.js"></script>
            <script src="<?php echo base_url(); ?>adminBSB/plugins/flot-charts/jquery.flot.resize.js"></script>
            <script src="<?php echo base_url(); ?>adminBSB/plugins/flot-charts/jquery.flot.pie.js"></script>
            <script src="<?php echo base_url(); ?>adminBSB/plugins/flot-charts/jquery.flot.categories.js"></script>
            <script src="<?php echo base_url(); ?>adminBSB/plugins/flot-charts/jquery.flot.time.js"></script>

        <!-- Sparkline Chart Plugin Js -->
        <script src="<?php echo base_url(); ?>adminBSB/plugins/jquery-sparkline/jquery.sparkline.js"></script>

        <?php }?>

        <!-- Custom Js -->
        <script src="<?php echo base_url(); ?>adminBSB/js/admin.js"></script>

        <?php if($page_current=='home'){  ?>

            <script src="<?php echo base_url(); ?>adminBSB/js/pages/index.js"></script>

        <?php }?>



    </body>

</html>