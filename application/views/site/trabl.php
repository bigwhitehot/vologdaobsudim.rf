<?php include('header.php'); ?>
    <div class="container">
        <div class="col-12 no-m">
            <div class="row">
                <div class="paging">
                    <?php echo $paging ?>
                </div>
                <div class="col-xl-12">
                    <h3 class="catalog">Каталог сообщений:</h3>
                </div>
                <?php foreach ($trabl as $t) : ?>
                    <?php if ($t->status == 2 || $t->status == 6 || $t->status == 10) : ?>
                    <div class="col-xl-4 col-sm-12 trabl">
                        <a class="trabl_a" href="<?= base_url('trabl/' . $t->id); ?>">
                            <span class="status"><?php trabl_status($t->status); ?></span>
                            <?php if ($t->file): ?>
                                <img src="<?= base_url('site/res/' . $t->file); ?>" />
                            <?php else: ?>
                                <img src="/assets/images/zagl_basic.png"/>
                            <?php endif; ?>

                            <div class="like">
                                <div class="ok" data-ids="<?= $t->id; ?>">
                                    <svg width="28" height="28" viewBox="0 0 28 28" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M9.33331 10.1967C9.33331 9.63968 9.53256 9.10106 9.89505 8.67816L15.4844 2.15721C15.9833 1.57517 16.8143 1.40736 17.5 1.75019C18.1694 2.0849 18.4908 2.86092 18.2542 3.57096L16.3333 9.33352H21.7041C21.8441 9.33352 21.9838 9.34612 22.1215 9.37116C23.3894 9.60168 24.2304 10.8164 23.9998 12.0843L22.515 20.2509C22.3133 21.3604 21.347 22.1669 20.2193 22.1669H11.6666C10.378 22.1669 9.33331 21.1222 9.33331 19.8335V10.1967Z"
                                              stroke="#43ac3e" stroke-linecap="round"></path>
                                        <path d="M4.66669 21V10.5" stroke="#43ac3e" stroke-linecap="round"></path>
                                    </svg>
                                    <span><?= trabl_ocenka($t->id, 0); ?></span>
                                </div>
                                <div class="fail" data-ids="<?= $t->id; ?>">
                                    <svg width="28" height="28" viewBox="0 0 28 28" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M9.33331 10.1967C9.33331 9.63968 9.53256 9.10106 9.89505 8.67816L15.4844 2.15721C15.9833 1.57517 16.8143 1.40736 17.5 1.75019C18.1694 2.0849 18.4908 2.86092 18.2542 3.57096L16.3333 9.33352H21.7041C21.8441 9.33352 21.9838 9.34612 22.1215 9.37116C23.3894 9.60168 24.2304 10.8164 23.9998 12.0843L22.515 20.2509C22.3133 21.3604 21.347 22.1669 20.2193 22.1669H11.6666C10.378 22.1669 9.33331 21.1222 9.33331 19.8335V10.1967Z"
                                              stroke="red" stroke-linecap="round"></path>
                                        <path d="M4.66669 21V10.5" stroke="red" stroke-linecap="round"></path>
                                    </svg>
                                    <span><?= trabl_ocenka($t->id, 1); ?></span>
                                </div>
                            </div>
                            <div class="info">
                                <small>
                                    <span>№ <?= $t->id; ?> <!--создано: <?= date('d.m.Y', strtotime($t->created_at)); ?>--></span>
                                </small><?= user($t->people_id)->firstname; ?>

                            </div>
                            <div class="addressico">
                                <svg width="8" height="12" viewBox="0 0 8 12" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4 0C1.73333 0 0 1.73333 0 4C0 6.26667 4 12 4 12C4 12 8 6.26667 8 4C8 1.73333 6.13333 0 4 0ZM4 5.33333C3.2 5.33333 2.66667 4.8 2.66667 4C2.66667 3.2 3.33333 2.66667 4 2.66667C4.8 2.66667 5.33333 3.33333 5.33333 4C5.33333 4.66667 4.8 5.33333 4 5.33333Z"
                                          fill="#43ac3e"></path>
                                </svg>
                                <?= str_replace('Россия, ', '', $t->address); ?>
                            </div>
                            <div class="text">
                                <?=mb_strimwidth($t->text, 0, 220, '...');?>
                            </div>
                        </a>
                    </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<div class="block-empty">&nbsp;</div>
<?php include('footer.php'); ?>