<div class="modal" id="authModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="<?= base_url('site/auth'); ?>">
                <div class="modal-header">
                    <h5 class="modal-title">Авторизация</h5>
                </div>
                <div class="modal-body">
                    <?php if (isset($_GET['auth'])) : ?>
                        <div class="text-center text-danger">
                            Неправильный логин или пароль!
                            </br>
                            </br>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" id="" placeholder="Почта" required="">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" id="" placeholder="Пароль"
                               required="">
                    </div>
                    <div id="html_element" class="text-center"></div>
                    <br>

                    <div class="vk-auth">
                        <?php echo $link = '<p><a href="' . $url . '?' . urldecode(http_build_query($params)) . '">Аутентификация через ВКонтакте</a></p>'; ?>
                    </div>
                    
                    <div class="text-center">
                        <a href="#" data-toggle="modal" data-dismiss="modal" data-target="#registerModal">Еще нет
                            аккаунта?</a>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-success">Войти</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="spsModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-center text-success">
                    Спасибо за ваше предложение! В ближайшее время мы его проверим!
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="registerModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="<?= base_url('site/register'); ?>">
                <div class="modal-header">
                    <h5 class="modal-title">Регистрация</h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" name="firstname" class="form-control" id="" placeholder="Имя" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="lastname" class="form-control" id="" placeholder="Фамилия" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="phone" class="form-control" id="" placeholder="Телефон" required>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" id="" placeholder="Почта" required>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" id="" placeholder="Пароль" required>
                    </div>
                    <div id="html_element2" class="text-center"></div>
                    <br>
                    <input type="checkbox" id="checkboxPersonalData">
                    <button type="submit" class="btn btn-success" disabled>Зарегистрироваться</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="mapModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="<?= base_url('site/send'); ?>" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title text-center" id="exampleModalLongTitle">Отправить предложение</h5>
                </div>
                <div class="modal-body">
                    <p class="text-center text-success">Заполните все поля для отправки предложения или проблемы.</p>
                    <p class="text-center" id="address"></p>
                    <input type="hidden" name="address" class="address" value=""/>
                    <input type="hidden" name="coord" class="coord" value=""/>
                    <div class="input-group mb-3">
                        <div class="custom-file" lang="ru">
                            <input type="file" class="custom-file-input" name="userfile">
                            <label class="custom-file-label" for="inputGroupFile01">Выберите фото</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="subject" required>
                            <option value="">Выберите тему обращения</option>
                            <?php foreach (subject() as $sub) : ?>
                                <option value="<?= $sub->id; ?>"><?= $sub->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="text" rows="4"
                                  placeholder="Опишите проблему или предложение ..." required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                    <button type="submit" class="btn btn-success">Отправить</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="footer">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-6 col-sm-12">
                <img class="logo-img-footer" style="width: 130px;" src="<?= base_url('assets/images/logo.png'); ?>">
                <p>При полном или частичном использовании материалов сайта ссылка на источник обязательна.
                    <br>Ваши замечания и пожелания по сайту ждём на hello@вологдаобсудим.рф
                    <br><b>© <?= date('Y'); ?> Городской портал Вологдаобсудим.рф Все права защищены.</b>
                </p>
            </div>
            <div class="col-xl-4 col-sm-12">

            </div>
            <div class="col-xl-2 col-sm-12">
                <a href="https://einstein-studio.ru" target="_blank" >
                <img class="logo-img" style="width: 130px;" src="<?= base_url('assets/images/logo_einstein.png'); ?>">
                </a>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        <?php if (isset($_GET['send']) && $_GET['send'] == 'ok' && $page == 'mytrabl') : ?>
        $('#spsModal').modal('show');
        <?php endif;?>
        <?php if (isset($_GET['auth'])) : ?>
        $('#authModal').modal('show');
        <?php endif; ?>

        //if($('#checkboxPersonalData').on('click'))
    })
</script>
</body>
</html>