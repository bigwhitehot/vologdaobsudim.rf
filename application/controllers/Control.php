<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Control extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
        $this->load->library('email');

        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
    }

    private function sendEmail($text, $subject, $userEmail)
    {

        $config_mail['protocol'] = 'smpt';
        $config_mail['mailpath'] = '/usr/sbin/sendmail';
        $config_mail['charset'] = 'iso-8859-1';
        $config_mail['wordwrap'] = TRUE;
        $config_mail['smpt_host'] = 'smtp.jino.ru';
        $config_mail['smtp_user'] = 'test@einstein-studio.ru';
        $config_mail['smtp_pass'] = ';jgjrhsk';
        

        $this->email->initialize($config_mail);

        $this->email->from('test@einstein-studio.ru', 'Support');
        $this->email->to('test@einstein-studio.ru');
        $this->email->subject($subject);
        $this->email->message($text);
        $this->email->send();
    }

    public function index()
    {
        if ($this->ion_auth->logged_in()) {
            if ($this->ion_auth->is_admin()) {
                redirect('control/trabl');
            } else {
                return show_error('You must be an administrator to view this page.');
            }
        } else {
            redirect('auth/login');
        }

        return show_error('Error');
    }

    public function users()
    {
        if (!$this->ion_auth->is_admin()) {
            return show_error('You must be admin');
        }

        $crudFields = array(
            "id" => "#",
            "username" => "Логин",
            "ip_address" => "IP",
            "first_name" => "Имя",
            "phone" => "Телефон",
            "last_name" => "Фамилия",
            "created_on" => "Зарег.",
            "last_login" => "Посл. вход",
            "active" => "Бан",
            "rules" => "Права",
            'category' => 'Категория ответственности'
        );

        $crud = new grocery_CRUD();
        $crud->set_table('ci_users');

        $crud->unset_columns('password', 'salt', 'forgotten_password_code', 'activation_code', 'forgotten_password_time', 'remember_code', 'company');
        $crud->unset_fields('password', 'salt', 'forgotten_password_code', 'activation_code', 'forgotten_password_time', 'remember_code', 'company', 'ip_address', 'created_on', 'last_login');

        $crud->callback_column('created_on', array($this, 'date_display'));
        $crud->callback_column('last_login', array($this, 'date_display'));

        $crud->set_relation('category', 'ci_trabl_subject', 'name');

        $crud->field_type('active', 'true_false', array('1' => 'Выключен', '0' => 'Включен'));

        $crud->set_relation_n_n('rules', 'ci_users_groups', 'ci_groups', 'user_id', 'group_id', 'description');

        $crud->required_fields('username', 'first_name', 'last_name', 'email');

        foreach ($crudFields as $index => $value) {
            $crud->display_as($index, $value);
        }

        $output = $crud->render();

        $this->template->load('template', 'control/users', $output, TRUE);
    }

    function trabl()
    {
        if (!$this->ion_auth->is_admin()) {
            return show_error('You must be admin');
        }

        $user = $this->ion_auth->user()->row();

        $crudFields = array(
            "id" => "id",
            "people_id" => "Пользователь",
            "address" => "Адрес",
            "file" => "Фото",
            "subject" => "Тема",
            "text" => "Текст",
            'message' => 'Ответ пользователю',
            "status" => "Статус",
            "created_at" => "Дата создания",
            "updated_at" => "Дата обновления",
        );

        $crud = new grocery_CRUD();

        if ($user->category != 0) {
            $crud->where('subject', $user->category);
        }

        $crud->set_table('ci_trabl');

        $crud->set_relation('people_id', 'ci_people', '{firstname} {lastname}');
        $crud->set_relation('subject', 'ci_trabl_subject', 'name');
        $crud->set_field_upload('file','uploads');

        $crud->columns('id', 'people_id', 'address', 'file', 'subject', 'text', 'status', 'created_at', 'updated_at');
        if(preg_match('/add/', base_url(uri_string()))) {
            $crud->set_relation('people_id', 'ci_users', '{first_name} {last_name}');
            $crud->add_fields('people_id', 'file', 'subject', 'text', 'status');
        }

        $crud->edit_fields('people_id', 'status', 'subject', 'text', 'message');

        $this->sendEmail('asd', 'asd', 'asd');

        $crud->unset_columns('coord');
        $crud->unset_fields('coord');

        $crud->field_type('status', 'dropdown', trabl_status());


        foreach ($crudFields as $index => $value) {
            $crud->display_as($index, $value);
        }

        $output = $crud->render();

        $this->template->load('template', 'control/users', $output, TRUE);
    }

    function comment()
    {

        if (!$this->ion_auth->is_admin()) {
            return show_error('You must be admin');
        }

        $crudFields = array(
            "id" => "#",
            "people_id" => "Пользователь",
            "trabl_id" => "Обращение",
            "text" => "Текст комментария",
            "created_at" => "Дата комментария"
        );
//
        $crud = new grocery_CRUD();
        $crud->set_table('ci_comment');

        $crud->columns('id', 'people_id', 'trabl_id', 'text', 'created_at');
        $crud->edit_fields('people_id', 'status', 'trabl_id', 'text', 'created_at');
//
        $crud->set_relation('people_id', 'ci_people', '{firstname} {lastname}');
//
        foreach ($crudFields as $index => $value) {
            $crud->display_as($index, $value);
        }


        //$this->sendEmail()
        $output = $crud->render();

        $this->template->load('template', 'control/users', $output, TRUE);
    }

    function trabl_subject()
    {
        $crudFields = array(
            "id" => "#",
            "name" => "Название темы",
        );

        $crud = new grocery_CRUD();
        $crud->set_table('ci_trabl_subject');

        foreach ($crudFields as $index => $value) {
            $crud->display_as($index, $value);
        }

        $output = $crud->render();

        $this->template->load('template', 'control/users', $output, TRUE);
    }

    function news()
    {

        if (!$this->ion_auth->is_admin()) {
            return show_error('You must be admin');
        }

        $crudFields = array(
            "id" => "#",
            "name" => "Название новости",
            "text" => "Текст новости",
            "metaTitle" => "metaTitle",
            "metaDescription" => "metaDescription",
            "photo" => "Изображение",
            "created_at" => "Дата создания",
        );

        $crud = new grocery_CRUD();
        $crud->set_table('ci_news');

        $crud->set_field_upload('photo','uploads');

        foreach ($crudFields as $index => $value) {
            $crud->display_as($index, $value);
        }

        $output = $crud->render();

        $this->template->load('template', 'control/users', $output, TRUE);
    }

    function people()
    {
        if (!$this->ion_auth->is_admin()) {
            return show_error('You must be admin');
        }


        $crudFields = array(
            "id" => "#",
            "firstname" => "Имя",
            "lastname" => "Фамилия",
            "phone" => "Телефон",
            "email" => "Email",
            "password" => "Пароль",
        );

        $crud = new grocery_CRUD();
        $crud->set_table('ci_people');

        $crud->unset_fields('secondname', 'created_at', 'password');
        $crud->unset_columns('secondname', 'created_at', 'password');

        foreach ($crudFields as $index => $value) {
            $crud->display_as($index, $value);
        }

        $output = $crud->render();

        $this->template->load('template', 'control/users', $output, TRUE);
    }

    function date_display($value, $row)
    {
        return date('d.m.Y', $value);
    }

    function tables()
    {
        $this->template->load('template', 'table');
    }

}
