<?php include('header.php'); ?>
    <div class="container content news-one">
        <h3><?= $news->name; ?></h3>
        <div class="author font-italic">Опубликованно <?= date('d.m.Y', strtotime($news->created_at)); ?></div>
        <p>
            <?= $news->text; ?>
        </p>
        <div class="social row">
	        <!-- uSocial -->
			<script async src="https://usocial.pro/usocial/usocial.js?v=6.1.4" data-script="usocial" charset="utf-8"></script>
			<div class="uSocial-Share" data-pid="1b4833b9cdb352f5bfa2aee07c1de5b0" data-type="share" data-options="rect,style4,default,absolute,horizontal,size48,eachCounter1,counter0" data-social="vk,fb,telegram,ok,print" data-mobile="vi,wa,sms"></div>
			<!-- /uSocial -->
		
		</div>
    </div>

    <div class="block-empty">&nbsp;</div>

    <div class="container-fluid news">
	    <div class="row">
	        <?php foreach ($last_news as $ln) : ?>
	            <div class="col-xl-4 col-sm-12">
	                <div class="news_home">
	                    <?php if ($ln->photo): ?>
	                        <img src="<?= base_url('site/res/' . $ln->photo); ?>" />
	                    <?php else: ?>
	                        <img src="/assets/images/zagl_basic.png"/>
	                    <?php endif; ?>
	                    <div class="date_news"><?=date('d.m.Y', strtotime($ln->created_at));?> |
	                        <?=date('h:i', strtotime($ln->created_at));?></div>
	                    <h3><?=mb_strimwidth($ln->name, 0, 26, '...');?></h3>
	                    <p>
	                        <?=mb_strimwidth($ln->text, 0, 220, '...');?>
	                    </p>
	                    <button class="btn readmore" onclick="window.location = '/news/<?=$ln->url;?>'">Читать далее...</button>
	                </div>
	            </div>
	        <?php endforeach; ?>
	    </div>
	</div>
	<div class="block-empty">&nbsp;</div>
<?php include('footer.php'); ?>