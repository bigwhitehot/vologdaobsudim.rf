<html>
<body>
<h1><?php echo sprintf(lang('email_forgot_password_heading'), $identity); ?></h1>
<p>
    <?php echo lang('email_forgot_password_subheading'); ?>
    <a href="https://erp.cit-llc.ru/<?= base_url('auth/reset_password/' . $forgotten_password_code); ?>">https://erp.cit-llc.ru/<?= base_url('auth/reset_password/' . $forgotten_password_code); ?></a>
</p>
</body>
</html>