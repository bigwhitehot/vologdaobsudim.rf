<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Ошибка 404</title>
    <!-- Favicon-->
    <link rel="icon" href="/adminBSB/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/adminBSB/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/adminBSB/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/adminBSB/css/style.css" rel="stylesheet">
</head>

<body class="four-zero-four">
<div class="four-zero-four-container">
    <div class="error-code">404</div>
    <div class="error-message">Запрошенная Вами страница не найдена.</div>
    <div class="button-place">
        <a href="/" class="btn btn-default btn-lg waves-effect">ВЕРНУТСЯ НА ГЛАВНУЮ</a>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="/adminBSB/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/adminBSB/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/adminBSB/plugins/node-waves/waves.js"></script>
</body>

</html>
