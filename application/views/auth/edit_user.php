<?php if ($message) { ?>
    <div class="callout callout-danger lead" id="infoMessage">
        <h4>Warning !</h4>
        <p><?php echo $message;?></p>
    </div>
<?php } ?>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2><?php echo lang('edit_user_subheading');?></h2>
            </div>
            <div class="body">
                <?php echo form_open(uri_string());?>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label"><?php echo lang('edit_user_fname_label', 'first_name');?></label>
                            <?php echo form_input($first_name, '', 'class="form-control"');?>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label"><?php echo lang('edit_user_lname_label', 'last_name');?></label>
                            <?php echo form_input($last_name, '', 'class="form-control"');?>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label"><?php echo lang('edit_user_company_label', 'company');?></label>
                            <?php echo form_input($company, '', 'class="form-control"');?>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label"><?php echo lang('edit_user_phone_label', 'phone');?></label>
                            <?php echo form_input($phone, '', 'class="form-control"');?>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label"><?php echo lang('edit_user_password_label', 'password');?></label>
                            <?php echo form_input($password, '', 'class="form-control"');?>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label"><?php echo lang('edit_user_password_confirm_label', 'password_confirm');?></label>
                            <?php echo form_input($password_confirm, '', 'class="form-control"');?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php if ($this->ion_auth->is_admin()): ?>
                        <?php foreach ($groups as $group):?>

                            <?php $checked = null;
                                foreach($currentGroups as $grp) {
                                    if ($group['id'] == $grp->id) {
                                        $checked = ' checked="checked"';
                                        break;
                                    }
                                }
                            ?>

                                <input type="checkbox" name="groups[]" id="checkbox<?php echo $group['id']?>" value="<?php echo $group['id']?>" class="filled-in" <?php echo $checked;?>>
                                <label for="checkbox<?php echo $group['id']?>"><?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?></label><br>

                        <?php endforeach?>
                        <?php endif ?>
                    </div>

                    <?php echo form_hidden('id', $user->id);?>
                    <?php echo form_hidden($csrf); ?>

                    <button class="btn btn-primary waves-effect" type="submit">СОХРАНИТЬ</button>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>