<html lang="ru-RU">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Вологдаобсудим.рф — Вологдаобсудим.рфU</title>

    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,400,700|Roboto:300,400,700&display=swap&subset=cyrillic"
          rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/css/site.css'); ?>" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="h-100">
    <div class="container">
        <div class="row align-items-center h-100">
            <div class="col-12 text-center">
                <img src="/assets/images/underconstruction.png"/>
            </div>
        </div>
    </div>
</div>
</body>
</html>