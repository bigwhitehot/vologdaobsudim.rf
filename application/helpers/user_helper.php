<?php

function user($id = 0)
{
    $ci = &get_instance();

    if ($id > 0) {
        $user = $ci->db->where('id', $id);
    } else {
        $user = $ci->db->where('id', $ci->session->userdata('user_id'));
    }

    $user = $ci->db->get('ci_people')->row_object();

    return $user ? $user : false;
}

function subject()
{
    $ci = &get_instance();

    $subject = $ci->db->get('ci_trabl_subject')->result_object();

    return $subject;
}

function trabl()
{
    $ci = &get_instance();

    $subject = $ci->db->get('ci_trabl')->result_object();

    return $subject;
}

function trabl_ocenka($trabl_id, $type)
{
    $ci = &get_instance();

    $subject = $ci->db->where(array(
        'trabl_id' => $trabl_id,
        'type' => $type
    ))->get('ci_trabl_ocenka')->num_rows();

    return $subject;
}

function trabl_status($id = '')
{
    $trabl_status = array(
        1 => 'на модерации',
        2 => 'новая',
        3 => 'отклонено',
        4 => 'в работе',
        5 => 'предварительно решено',
        6 => 'решено',
        7 => 'отложено',
        8 => 'запланировано',
        9 => 'удалено',
        10 => 'в архиве',
    );

    if (!empty($id)) {
        echo $trabl_status[$id];
    } else {
        return $trabl_status;
    }
}

function crop_image($path)
{
    $ci = &get_instance();

    $ci->image_lib->clear();

    $config['image_library'] = 'GD2';
//    $config['library_path'] = '/usr/bin/convert';
    $config['create_thumb'] = FALSE;
    $config['source_image'] = getcwd() . $path;
    $config['maintain_ratio'] = TRUE;
    $config['dynamic_output'] = TRUE;
    $config['width'] = 300;
    $config['height'] = 200;

    $ci->image_lib->initialize($config);

    if (!$ci->image_lib->resize()) {
        var_dump($ci->image_lib->display_errors());
    }
}