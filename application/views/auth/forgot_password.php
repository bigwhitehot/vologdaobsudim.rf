<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Восстановить пароль | Панель управления</title>
    <!-- Favicon-->
    <link rel="icon" href="/adminBSB/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/adminBSB/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/adminBSB/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/adminBSB/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/adminBSB/css/style.css" rel="stylesheet">
</head>

<body class="fp-page">
<div class="fp-box">
    <div class="logo">
        <a href="/">Admin Panel<b> TARGET</b></a>
        <small>Панель управления сервисом Target</small>
    </div>
    <div class="card">
        <div class="body">
            <?php echo form_open("auth/forgot_password");?>
                <div class="msg">
                    <h1><?php echo lang('forgot_password_heading');?></h1>
                    <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
                    <?php echo $message;?>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                    <div class="form-line">

                        <?php echo form_input($identity, '', 'placeholder="Email"');?>
                    </div>
                </div>

                <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">ВОССТАНОВИТЬ ПАРОЛЬ</button>

                <div class="row m-t-20 m-b--5 align-center">
                    <a href="/auth/login">Вернутся на страницу входа!</a>
                </div>
             <?php echo form_close();?>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="/adminBSB/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/adminBSB/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/adminBSB/plugins/node-waves/waves.js"></script>

<!-- Validation Plugin Js -->
<script src="/adminBSB/plugins/jquery-validation/jquery.validate.js"></script>

<!-- Custom Js -->
<script src="/adminBSB/js/admin.js"></script>
<script src="/adminBSB/js/pages/examples/forgot-password.js"></script>
</body>

</html>
