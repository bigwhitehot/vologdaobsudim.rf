<?php


class Site extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        $this->load->library('ion_auth');


        if (WORK_STR == true && $this->router->fetch_method() != 'underconstruction') {
            redirect('/site/underconstruction');
        }
    }

    private function sendEmail($text, $subject)
    {

        $config_mail['protocol'] = 'smpt';
        $config_mail['mailpath'] = '/usr/sbin/sendmail';
        $config_mail['charset'] = 'iso-8859-1';
        $config_mail['wordwrap'] = TRUE;
        $config_mail['smpt_host'] = 'smtp.jino.ru';
        $config_mail['smtp_user'] = 'test@einstein-studio.ru';
        $config_mail['smtp_pass'] = ';jgjrhsk';
        

        $this->email->initialize($config_mail);

        $this->email->from('test@einstein-studio.ru', 'Support');
        $this->email->to('test@einstein-studio.ru');
        $this->email->subject($subject);
        $this->email->message($text);
        $this->email->send();
    }

    public function index()
    {
        $total_user = $this->db->get('ci_people')->num_rows();
        $total_trabl = $this->db->get('ci_trabl')->num_rows();
        $last_news = $this->db->order_by('id',"desc")->limit(3)->get('ci_news')->result_object();

        $config_vk = $this->load->config('vk_auth');
        $url = $this->config->item('VK_AUTH_OAUTH_URL');
        $params = $this->config->item('VK_AUTH_PARAMS');

        $this->load->view('site/index', array(
            'total_user' => $total_user,
            'total_trabl' => $total_trabl,
            'last_news' => $last_news,
            'url' => $url,
            'params' => $params,
        ));
    }

    public function auth()
    {
        $post = $this->input->post();

        if ($post) {
            $user = $this->db->where(array(
                'email' => $post['email'],
                'password' => md5($post['password'])
            ))->get('ci_people')->row_object();

            if (!empty($user)) {
                $this->session->set_userdata('user_id', $user->id);

                redirect('/');
            } else {
                redirect('/?auth=fail');
            }
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('user_id');
        $this->ion_auth->logout();

        redirect('/');
    }

    public function register()
    {
        $post = $this->input->post();

        if ($post) {
            $user = $this->db->where('email', $post['email'])->get('ci_people')->row_object();

            if (empty($user)) {
                $post['password'] = md5($post['password']);
                unset($post['g-recaptcha-response']);

                $this->db->insert('ci_people', $post);

                $last_id = $this->db->insert_id();

                $this->session->set_userdata('user_id', $last_id);

                redirect('/#map');
            } else {

            }
        }
    }

    public function send()
    {
        $post = $this->input->post();
        $this->load->library('image_lib');

        if ($post) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 1000;
            $config['max_width'] = 5000;
            $config['max_height'] = 5000;

            $this->load->library('upload', $config);

            $post['people_id'] = $this->session->userdata('user_id');
            $post['created_at'] = date('Y-m-d');
            $post['status'] = 1;
            unset($post['userfile']);

            if ($this->upload->do_upload('userfile')) {
                $data = $this->upload->data();
                $post['file'] = $data['file_name'];
            }

            print_r($post);
            die();

            $config_image['image_library'] = 'gd2';
            $config_image['source_image'] = $config['upload_path'] . $this->upload->data('file_name');
            $config_image['maintain_ratio'] = TRUE;
            $config_image['width'] = 750;
            $config_image['height'] = 500;

            $this->load->library('image_lib', $config_image);

            $this->image_lib->resize();

            $this->db->insert('ci_trabl', $post);

            $last_id = $this->db->insert_id();

            redirect('/site/mytrabl/?send=ok');
        }
    }

    public function news($url = '')
    {
        $last_news = $this->db->order_by('id',"desc")->limit(3)->get('ci_news')->result_object();
        $config_vk = $this->load->config('vk_auth');
        $vk_url = $this->config->item('VK_AUTH_OAUTH_URL');
        $params = $this->config->item('VK_AUTH_PARAMS');

        if (empty($url)) {
            $news = $this->db->get('ci_news')->result_object();

            $this->load->view('site/news', array(
                'news' => $news,
                'last_news' => $last_news,
                'url' => $vk_url,
                'params' => $params,
            ));
        } else {
            $news = $this->db->where('url', $url)->get('ci_news')->row_object();

            $this->load->view('site/news_one', array(
                'news' => $news,
                'last_news' => $last_news,
                'url' => $vk_url,
                'params' => $params,
            ));
        }
    }

    public function paging($page = 0) {
        $this->load->library('pagination');
        $config_vk = $this->load->config('vk_auth');
        $url = $this->config->item('VK_AUTH_OAUTH_URL');
        $params = $this->config->item('VK_AUTH_PARAMS');

        $troubles = $this->db->get('ci_trabl', 9, $page)->result_object();

        $config['base_url'] = base_url() . 'trabl/page';
        $config['total_rows'] = $this->db->count_all_results('ci_trabl');
        $config['per_page'] = 9;
        $config['first_url'] = base_url() . 'trabl';
        $config['first_link'] = 'Первая ';
        $config['last_link'] = ' Последняя';
        $config['next_link'] = '';
        $this->pagination->initialize($config);

        $paging = $this->pagination->create_links();

        $this->load->view('site/trabl', array(
            'trabl' => $troubles,
            'paging' => $paging,
            'url' => $url,
            'params' => $params,
        ));
    }

    public function trabl($id = 0)
    {
        $config_vk = $this->load->config('vk_auth');
        $url = $this->config->item('VK_AUTH_OAUTH_URL');
        $params = $this->config->item('VK_AUTH_PARAMS');

        if ($id == 0) {
            $trabl = $this->db->where('status >', 1)->get('ci_trabl')->result_object();

            $this->load->view('site/trabl', array(
                'trabl' => $trabl,
                'url' => $url,
                'params' => $params,
            ));
        } else {
            $trabl = $this->db->where('id', $id)->get('ci_trabl')->row_object();
            $comment = $this->db->where('trabl_id', $trabl->id)->get('ci_comment')->result_object();

            $this->load->view('site/trabl_one', array(
                'trabl' => $trabl,
                'comment' => $comment,
                'url' => $url,
                'params' => $params,
            ));
        }
    }

    public function mytrabl($id = 0)
    {
        $trabl = $this->db->where('people_id', user()->id)->get('ci_trabl')->result_object();
        $config_vk = $this->load->config('vk_auth');
        $url = $this->config->item('VK_AUTH_OAUTH_URL');
        $params = $this->config->item('VK_AUTH_PARAMS');

        $this->load->view('site/trabl', array(
            'trabl' => $trabl,
            'paging' => '',
            'url' => $url,
            'params' => $params,
        ));
    }

    public function comment($trabl_id)
    {
        $post = $this->input->post();

        if ($post) {
            $this->db->insert('ci_comment', array(
                'people_id' => user()->id,
                'trabl_id' => $trabl_id,
                'text' => $post['text'],
            ));

            $insert_id = $this->db->insert_id();

            $this->sendEmail($post['text'], 'Comment');

            redirect('/trabl/' . $trabl_id . '#com' . $insert_id);
        }
    }

    public function ajax_trabl()
    {
        $trabl = $this->db->where('status >', 1)->get('ci_trabl')->result_object();

        $features = array();

        $i = 0;

        foreach ($trabl as $t) {
            $img = $t->file ? '<img height="200px;" src="./uploads/' . $t->file . '" /></br>' : '';

            $features[] = array(
                'type' => 'Feature',
                'id' => $i,
                'geometry' => array(
                    'type' => 'Point',
                    'coordinates' => explode(',', $t->coord),
                ),
                'properties' => array(
                    'balloonContentBody' => $img . $t->text,
                    'balloonContentFooter' => '<a href="/trabl/' . $t->id . '">перейти в предложение</a>',
                    'clusterCaption' => 'Обращение №' . $t->id,
                )
            );

            $i++;
        }

        echo json_encode(array(
            'type' => 'FeatureCollection',
            'features' => $features
        ));
    }

    public function ajax_reit()
    {
        $get = $this->input->get();

        if ($get['type'] == 'ok') {
            $type = 0;
        } else {
            $type = 1;
        }

        $user = $this->db->where(array(
            'people_id' => $this->session->userdata('user_id'),
            'trabl_id' => $get['trabl_id']
        ))->get('ci_trabl_ocenka')->row_object();

        if (empty($user) && $this->session->userdata('user_id') > 0) {
            $status = 'ok';

            $this->db->insert('ci_trabl_ocenka', array(
                'people_id' => $this->session->userdata('user_id'),
                'trabl_id' => $get['trabl_id'],
                'type' => $type
            ));
        } else {
            $status = 'fail';
        }

        echo json_encode(array(
            'status' => $status
        ));
    }

    public function underconstruction() {
        $this->load->view('site/underconstruction');
    }

    public function res ($name) {
        $this->image_lib->clear();

        $config['image_library'] = 'GD2';
        $config['source_image'] = './uploads/'.$name;
        $config['dynamic_output'] = TRUE;
        $config['width'] = 300;
        $config['height'] = 200;

        $this->image_lib->initialize($config);

        if (!$this->image_lib->resize()) {
            var_dump($this->image_lib->display_errors());
        }
    }

    public function vk_auth()
    {

        $this->config->load('vk_auth');

        $client_id = $this->config->item('VK_AUTH_APPID');
        $secret = $this->config->item('VK_AUTH_SECRET');
        $redirect_uri = $this->config->item('VK_AUTH_REDIRECT_URL');

        $userInfo = [];
        $token = '';

        if(!empty($code = $this->input->get('code')))
        {
            $params = array(
                'client_id' => $client_id,
                'client_secret' => $secret,
                'code' => $code,
                'redirect_uri' => $redirect_uri
            );      
                        
            $token = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))), true);
            
            if(isset($token['access_token']))
            {
                $params = array(
                    'uids' => $token['user_id'],
                    'fields' => 'uid,first_name,last_name',
                    'access_token' => $token['access_token'],
                    'v' => '5.103'
                );

                $token = $token['access_token'];
                $userInfo = json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($params))), true);
            }
        }

        $user = $this->db->where(array(
            'uid' => $userInfo['response'][0]['id']
        ))->get('ci_vk_users')->row_object();

        if (is_null($user)) {

            $this->db->insert('ci_people', array(
                'firstname' => $userInfo['response'][0]['first_name'],
                'lastname' => $userInfo['response'][0]['last_name'],
                'password' => $token
            ));

            $last_id = $this->db->insert_id();

            $new_vk_user = [
                'first_name' => $userInfo['response'][0]['first_name'],
                'last_name' => $userInfo['response'][0]['last_name'],
                'uid' => $userInfo['response'][0]['id'],
                'access_token' => $token,
                'people_id' => $last_id
            ];

            $this->db->insert('ci_vk_users', $new_vk_user);

            $this->session->set_userdata('user_id', $last_id);

            redirect('/');
        } else {

            $this->session->set_userdata('user_id', $user->people_id);

            redirect('/');
        }
    }
}