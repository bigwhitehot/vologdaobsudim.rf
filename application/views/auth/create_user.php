<?php if ($message) { ?>
<div class="callout callout-danger lead" id="infoMessage">
  <h4>Warning !</h4>
  <p><?php echo $message;?></p>
</div>
<?php } ?>

<?php if ($message) { ?>
    <div class="callout callout-danger lead" id="infoMessage">
        <h4>Warning !</h4>
        <p><?php echo $message;?></p>
    </div>
<?php } ?>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2><?php echo lang('create_user_subheading');?></h2>
            </div>
            <div class="body">
                <?php echo form_open("user_keys/create_user", "role='form'");?>
                <div class="form-group form-float">
                    <div class="form-line">
                        <label class="form-label"><?php echo lang('create_user_fname_label', 'first_name');?></label>
                        <?php echo form_input($first_name, '', 'class="form-control"');?>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <label class="form-label"><?php echo lang('create_user_lname_label', 'last_name');?></label>
                        <?php echo form_input($last_name, '', 'class="form-control"');?>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <label class="form-label"><?php echo lang('create_user_company_label', 'company');?></label>
                        <?php echo form_input($company, '', 'class="form-control"');?>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <label class="form-label"><?php echo lang('create_user_email_label', 'email');?></label>
                        <?php echo form_input($email, '', 'class="form-control"');?>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <label class="form-label"><?php echo lang('create_user_phone_label', 'phone');?></label>
                        <?php echo form_input($phone, '', 'class="form-control"');?>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <label class="form-label"><?php echo lang('create_user_password_label', 'password');?></label>
                        <?php echo form_input($password, '', 'class="form-control"');?>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <label class="form-label"><?php echo lang('create_user_password_confirm_label', 'password_confirm');?></label>
                        <?php echo form_input($password_confirm, '', 'class="form-control"');?>
                    </div>
                </div>

                <button class="btn btn-primary waves-effect" type="submit">ДОБАВИТЬ</button>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>