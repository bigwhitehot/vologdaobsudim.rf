
<!-- Basic Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Пользователи
                    <!-- <small>Basic example without any additional modification classes</small> -->
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="/user_keys/create_user">Добавить пользователя</a></li>
                            <li><a href="/user_keys/create_group">Добавить группу</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo lang('index_fname_th');?></th>
                        <th><?php echo lang('index_lname_th');?></th>
                        <th><?php echo lang('index_email_th');?></th>
                        <th><?php echo lang('index_groups_th');?></th>
                        <th><?php echo lang('index_status_th');?></th>
                        <th><?php echo lang('index_action_th');?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($users as $user):?>
                        <tr>
                            <td><?php echo htmlspecialchars($user->id,ENT_QUOTES,'UTF-8');?></td>
                            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
                            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
                            <td>
                                <a href="mailto:<?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?>">
                                    <?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?>
                                </a>
                            </td>
                            <td>
                                <?php foreach ($user->groups as $group):?>
                                    <?php echo anchor("/user_keys/edit_group/".$group->id, htmlspecialchars($group->description,ENT_QUOTES,'UTF-8'), 'class="btn btn-info"') ;?><br />
                                <?php endforeach?>
                            </td>
                            <td><?php echo ($user->active) ?
                                    anchor("/user_keys/deactivate/". $user->id, lang('index_active_link'), 'class="btn btn-success"')
                                    :
                                    anchor("/user_keys/activate/". $user->id, lang('index_inactive_link'), 'class="btn btn-danger"');?></td>
                            <td><?php echo anchor("/user_keys/edit_user/".$user->id, 'Редактировать', 'class="btn btn-primary"') ;?></td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>