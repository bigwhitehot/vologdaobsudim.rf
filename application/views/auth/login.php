<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Авторизация | Панель управления</title>
    <!-- Favicon-->
    <link rel="icon" href="/adminBSB/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/adminBSB/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/adminBSB/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/adminBSB/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/adminBSB/css/style.css" rel="stylesheet">
</head>

<body class="login-page">
<div class="login-box">
    <div class="logo">
        <a href="/"><b>Admin Panel</b></a>
        <small>Панель управления сервисом <?php echo config_item('project_name'); ?></small>
    </div>
    <div class="card">
        <div class="body">
            <?php echo form_open("auth/login");?>
                <div class="msg">Войдите, чтобы начать сессию</div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                    <div class="form-line">
                        <?php echo form_input($identity, '', 'type="email" class="form-control" placeholder="Email"');?>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        <?php echo form_input($password, '', 'type="password" class="form-control" placeholder="Пароль"');?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-8 p-t-5">
                        <input type="checkbox" name="remember" id="rememberme" class="filled-in chk-col-pink">
                        <label for="rememberme">Запомнить меня</label>
                    </div>
                    <div class="col-xs-4">
                        <?php echo form_submit('submit', lang('login_submit_btn'), 'class="btn btn-block bg-pink waves-effect"');?>
                    </div>
                </div>
                <div class="row m-t-15 m-b--20">
                    <div class="col-xs-6">
                       <!--  <a href="sign-up.html">Register Now!</a>-->
                    </div>
                    <div class="col-xs-6 align-right">
                        <a href="/auth/forgot_password">Забыли пароль?</a>
                    </div>
                </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="/adminBSB/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/adminBSB/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/adminBSB/plugins/node-waves/waves.js"></script>

<!-- Validation Plugin Js -->
<script src="/adminBSB/plugins/jquery-validation/jquery.validate.js"></script>

<!-- Custom Js -->
<script src="/adminBSB/js/admin.js"></script>
<script src="/adminBSB/js/pages/examples/sign-in.js"></script>
</body>

</html>