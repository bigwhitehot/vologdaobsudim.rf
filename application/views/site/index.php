<?php include('header.php'); ?>

<div class="container content">
    <div class="row">
        <div class="col-xl-12 col-sm-12">
            <h1>Как пользоваться площадкой?</h1>
            <div class="row small-pr-items">
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="small-pr-item">
                        <div class="small-pr-item__image">

                        </div>

                        <div class="small-pr-item__number">
                            <div class="circle">
                                1
                            </div>
                        </div>

                        <div class="small-pr-item__desc">
                            Регистрируйся через<br>соц.сети или электронную почту.
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="small-pr-item">
                        <div class="small-pr-item__image">

                        </div>

                        <div class="small-pr-item__number">
                            <div class="circle">
                                2
                            </div>
                        </div>

                        <div class="small-pr-item__desc">
                            Вноси свои предложения и<br>cообщай о проблемах в городе, просто указав их на карте ниже.
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="small-pr-item">
                        <div class="small-pr-item__image">

                        </div>

                        <div class="small-pr-item__number">
                            <div class="circle">
                                3
                            </div>
                        </div>

                        <div class="small-pr-item__desc">
                            Получай обратную связь и следи за ходом выполнения работ по сообщению.
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="small-pr-item">
                        <div class="small-pr-item__image">

                        </div>

                        <div class="small-pr-item__number">
                            <div class="circle">
                                4
                            </div>
                        </div>

                        <div class="small-pr-item__desc">
                            Оценивай нововведения и изменения в городе и голосуй за проекты.
                        </div>
                    </div>
                </div>
                <!--
                <div class="col-12 col-md-12 col-lg-12">
                    <p class="info_area">Более подробно о работе сервиса можно
                        узнать в <a href="" style="text-decoration: underline;color: #c33;">регламенте приема и
                            обработки сообщений.</a>
                    </p>
                </div>
                -->
            </div>
        </div>
    </div>
</div>

<div class="block-empty">&nbsp;</div>

<div class="map" id="map"></div>
<div class="block-empty">&nbsp;</div>

<div class="container content">
    <h1>Будущее Вологды зависит от тебя!</h1>
    <p>
        "Вологжане заинтересованы в принятии конструктивных решений по волнующим вопросам. Жители города выступают с
        предложениями по благоустройству парковых зон и дворов, ремонту дорог, строительству детских садов, улучшению
        работы управляющих компаний и даже совершенствованию законодательства. Именно инициативы горожан становятся
        основой проектов по улучшению качества жизни.
        Сайт Вологдаобсудим.рф - это онлайн-площадка, где каждый житель города сможет оставить свое предложение. Каждая
        инициатива будет направлена на рассмотрение экспертам в кратчайшие сроки.
        Рекомендую всем неравнодушным вологжанам принять участие в разработке конструктивных предложений по улучшению
        качества жизни на сайте Вологдаобсудим.рф"
    <div class="author text-right font-italic">Сергей Воропанов. Мэр города Вологды</div>
    </p>
</div>
<div class="block-empty">&nbsp;</div>

<div class="container-fluid news">
    <div class="row">
        <?php foreach ($last_news as $ln) : ?>
            <div class="col-xl-4 col-sm-12">
                <div class="news_home">
                    <?php if ($ln->photo): ?>
                        <img src="<?= base_url('site/res/' . $ln->photo); ?>" />
                    <?php else: ?>
                        <img src="/assets/images/zagl_basic.png"/>
                    <?php endif; ?>
                    <div class="date_news">
                        <small>
                            <span>
                                <?=date('d.m.Y', strtotime($ln->created_at));?> |
                        <?=date('h:i', strtotime($ln->created_at));?>
                            </span>
                        </small>
                    </div>
                    <h3><?=mb_strimwidth($ln->name, 0, 26, '...');?></h3>
                    <p>
                        <?=mb_strimwidth($ln->text, 0, 220, '...');?>
                    </p>
                    <button class="btn readmore" onclick="window.location = '/news/<?=$ln->url;?>'">Читать далее...</button>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<div class="block-empty">&nbsp;</div>
<!--
<div class="container content">
    <div class="row">
        <div class="col-xl-3 col-sm-12">
            <img src="<?= base_url('assets/images/img_200.png'); ?>" alt="..." class="img-thumbnail">
        </div>
        <div class="col-xl-3 col-sm-12">
            <img src="<?= base_url('assets/images/img_200.png'); ?>" alt="..." class="img-thumbnail">
        </div>
        <div class="col-xl-3 col-sm-12">
            <img src="<?= base_url('assets/images/img_200.png'); ?>" alt="..." class="img-thumbnail">
        </div>
        <div class="col-xl-3 col-sm-12">
            <img src="<?= base_url('assets/images/img_200.png'); ?>" alt="..." class="img-thumbnail">
        </div>
        <div class="col-xl-3 col-sm-12">
            <img src="<?= base_url('assets/images/img_200.png'); ?>" alt="..." class="img-thumbnail">
        </div>
        <div class="col-xl-3 col-sm-12">
            <img src="<?= base_url('assets/images/img_200.png'); ?>" alt="..." class="img-thumbnail">
        </div>
        <div class="col-xl-3 col-sm-12">
            <img src="<?= base_url('assets/images/img_200.png'); ?>" alt="..." class="img-thumbnail">
        </div>
        <div class="col-xl-3 col-sm-12">
            <img src="<?= base_url('assets/images/img_200.png'); ?>" alt="..." class="img-thumbnail">
        </div>
    </div>

</div>
-->
<?php include('footer.php'); ?>
